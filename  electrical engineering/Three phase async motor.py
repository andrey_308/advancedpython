import matplotlib.pyplot as plt
from math import sqrt
import numpy as np

n0 = 750
Mmax = 1313.125
Sn = 0.04
Skr = Sn * (2.2 + sqrt(2.2**2 - 1))
nkr = n0*(1 - Skr)
R2 = (596.875*0.105*750*Sn)/(3*190**2)

s_list = [i for i in np.arange(0.001, 1, 0.001)]
s2_list = [i for i in np.arange(0.001, 1, 0.01)]

n = []
M = []
I2 = []

for i, s in enumerate(s_list):
    n.append(n0*(1-s))
    M.append((2*Mmax)/((s/Skr)+(Skr/s)))
    I2.append(sqrt((M[i]*0.105*n0*s)/(3*R2)))

ax_01 = plt.axes()
line_01 = ax_01.plot(M, n)
ax_01.axis([0.0, 2000.0, 0.0, 750.0])
ax_01.set_xlabel(u'Момент, Нм')
ax_01.set_ylabel(u'Частота, об/мин')
ax_02 = ax_01.twiny()
line_02 = ax_02.plot(I2, n)
ax_02.set_xlabel(u'Ток, А')
ax_02.axis([0.0, 900.0, 0.0, 750.0])
ax_01.scatter(596.875, 720)
ax_02.scatter(190, 720)
ax_01.scatter(1313.125, 625.2)
ax_02.scatter(574.76, 625.2)

plt.show()

