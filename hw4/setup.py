from distutils.core import setup, Extension

setup(
    name = 'Matrix', 
    version = '1.0',
    ext_modules = [
        Extension('Matrix', ['Matrix.c'])
    ]
)