from Matrix import Matrix
from matplotlib import pyplot as plt
from random import randint
from time import time

class Matrix_py:
    def __init__(self, mtrx):
        num = len(mtrx[0])
        for row in mtrx[1:]:
            if len(row) != num:
                raise Exception("Rows not equal!")
        else:
            self.mtrx = mtrx 

    def __mul__(self, other_mtrx):
        if len(other_mtrx.mtrx) != len(self.mtrx[0]):
            raise Exception("First cols should equal secod rows!")
        else:
            s=0
            t=[]
            m3 = []
            r1=len(self.mtrx) #количество строк в первой матрице
            c1=len(self.mtrx[0]) #Количество столбцов в 1
            c2=len(other_mtrx.mtrx[0])  # количество столбцов во 2ой матрице
            for z in range(0,r1):
                for j in range(0,c2):
                    for i in range(0,c1):
                        s=s+self.mtrx[z][i]*other_mtrx.mtrx[i][j]
                    t.append(s)
                    s=0
                m3.append(t)
                t=[]           
        return Matrix_py(m3)

row_num = 8
col_num = 6
mtrx_c1 = []
mtrx_py1 = []
mtrx_c2 = []
mtrx_py2 = []
c_time = []
py_time = []
num_cycle = [10**i for i in range(7)]

for n in num_cycle:
    for i in range(n):
        mtrx_c1.append(Matrix([[randint(0,10000) for _ in range(col_num)] for x in range(row_num)]))
        mtrx_py1.append(Matrix_py([[randint(0,10000) for _ in range(col_num)] for x in range(row_num)]))
        mtrx_c2.append(Matrix([[randint(0,10000) for _ in range(row_num)] for x in range(col_num)]))
        mtrx_py2.append(Matrix_py([[randint(0,10000) for _ in range(row_num)] for x in range(col_num)]))

    strt = time()
    for i, a in enumerate(mtrx_c1):
        a*mtrx_c2[i]
    end = time()

    c_time.append(end-strt)

    strt1 = time()
    for i, a in enumerate(mtrx_py1):
        a*mtrx_py2[i]
    end1 = time()

    py_time.append(end1-strt1)

plt.xlabel("Количество операций")
plt.ylabel("Время, сек")
plt.plot(num_cycle, c_time, color="C0")
plt.plot(num_cycle, py_time, color="C1")
plt.legend ( ("C", "Python") )


plt.show()
#     _ _m_ _     _ _n_ _
#    |           |  
# l  |          m| 
#    |           |