#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include "structmember.h"
#include "stdlib.h"
#include <string.h>
#define ADD 1
#define MULTIPLY 2
#define NUM_MULTIPLY 3
#define DIVIDE 4

typedef struct {
    PyObject_HEAD
    long** mtrx;
    long row_num;
    long col_num;
} Matrix;

int len(char * s)
{
    int i;
    for(i = 0; s[i] != '\0'; i++);
    return i + 1;
}

void reverse(char s[])
{
    int i, j;
    char c;

    for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

int itoa(int n, char s[])
{
    int i, sign;

    if ((sign = n) < 0)
        n = -n;          
    i = 0;
    do {     
        s[i++] = n % 10 + '0';  
    } while ((n /= 10) > 0);     
    if (sign < 0)
        s[i++] = '-';
    s[i] = '|';
    i++;
    s[i] = '\0';
    reverse(s);
    return i + 1;
}

static int
Matrix_init(Matrix *self, PyObject *args, PyObject *kwgs)
{
    PyObject *mtrx;
    //PyObject_Print(args, stdout, Py_PRINT_RAW);
    self->col_num = 0;
    if (!PyArg_ParseTuple(args, "O", &mtrx)){
        return -1;
    }
    
    long row_index = PyList_Size(mtrx);
    long col_length;
    self->row_num = row_index;

    self->mtrx = (long**)malloc(row_index * sizeof(long*));

    for(long i = 0; i < row_index; i++){
        PyObject* temp_list = PyList_GetItem(mtrx, i);
        col_length = PyList_Size(temp_list);
        if(col_length != self->col_num && self->col_num != 0) {
            PyErr_SetString(PyExc_TypeError, "Rows not equal!");
                return -1;
        }
        self->col_num = col_length;
        self->mtrx[i] = (long*)malloc(col_length * sizeof(long));

        for (long j = 0; j < col_length; j++) {
            PyObject* temp = PyList_GetItem(temp_list, j);
            long elem = PyLong_AsLong(temp);
            self->mtrx[i][j] = elem;
        }
        
    }

    return 0;
}

static PyObject *
Matrix_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    Matrix *self;
    //PyObject_Print(type, stdout, Py_PRINT_RAW);
    self = (Matrix *) type->tp_alloc(type, 0);
    if (self != NULL) {
        self->mtrx = NULL;
        self->col_num = 0;
        self->row_num = 0;
    }
    return (PyObject *) self;
}

static PyObject *
Matrix_repr(Matrix * self)
{
    if (self->row_num == 0) {
        return PyUnicode_FromString("<Matrix {}>");
    }

    long temp;
    char buffer[12]; // Максимальное количество цифр в числе для long
    
    char * mtrx_str = (char *) malloc (12*sizeof(char));
    strcpy(mtrx_str, "<Matrix {\n\t");
    for(int i = 0; i < self->row_num; i++){
        
        for (int j = 0; j < self->col_num; j++) {
            temp = self->mtrx[i][j];
            int buffer_len = itoa(temp, buffer);
            mtrx_str = (char *) realloc (mtrx_str, len(mtrx_str) + buffer_len);
            mtrx_str = strcat(mtrx_str, buffer);
        }
        
        mtrx_str = (char *) realloc (mtrx_str, len(mtrx_str) + sizeof("|\n\t"));
        mtrx_str = strcat(mtrx_str, "|\n\t");

    }

    mtrx_str = (char *) realloc (mtrx_str, len(mtrx_str) + sizeof("}>"));
    mtrx_str = strcat(mtrx_str, "}>");

    return PyUnicode_FromString(mtrx_str);
}

static PyObject *
Matrix_str(Matrix * self)
{
    if (self->row_num == 0) {
        return PyUnicode_FromString("");
    }

    long temp;
    char buffer[12]; // Максимальное количество цифр в числе для long
    
    char * mtrx_str = (char *) malloc (12*sizeof(char));
    strcpy(mtrx_str, "");
    for(int i = 0; i < self->row_num; i++){
        
        for (int j = 0; j < self->col_num; j++) {
            temp = self->mtrx[i][j];
            int buffer_len = itoa(temp, buffer);
            mtrx_str = (char *) realloc (mtrx_str, len(mtrx_str) + buffer_len);
            mtrx_str = strcat(mtrx_str, buffer);
        }
        
        mtrx_str = (char *) realloc (mtrx_str, len(mtrx_str) + sizeof("|\n"));
        mtrx_str = strcat(mtrx_str, "|\n");

    }

    int i;
    for (i = 0; mtrx_str[i] != '\0'; i++);
    mtrx_str[i-1] = '\0';

    return PyUnicode_FromString(mtrx_str);
}

static PyObject *
Matrix_subscript(Matrix *self, PyObject *item) 
{
    long coordinate[] = {0, 0};
    long row_index;
    PyObject * temp;
    PyObject * index, * result;
    PyObject * new_list = PyList_New(self->col_num);
    
    if (PyIndex_Check(item)) {
        
        index = PyNumber_Index(item);
        if (!index)
            return NULL;
        row_index = PyLong_AS_LONG(index);
        
        if(row_index > self->col_num) {
            PyErr_SetString(PyExc_IndexError, "You have gone beyond the array!");
            return NULL;
        }

        for(int i = 0; i < self->col_num; i++) {
            temp = PyLong_FromLong(self->mtrx[row_index][i]);
            PyList_SetItem(new_list, i, temp);
        }

        result = Py_BuildValue("O", new_list);
        Py_DECREF(index);
        return result;
    }
    if (PyTuple_Check(item)) {
        int tuple_size = PyTuple_Size(item);
        if(tuple_size > 2){
            PyErr_SetString(PyExc_ValueError, "You're working with 2D matrix: use 2 coordinates!");
                return NULL;
        }
        for(int i = 0; i < tuple_size; i++) {
            temp = PyTuple_GetItem(item, i);
            if(!PyLong_Check(temp)) {
                PyErr_SetString(PyExc_TypeError, "coordinate items must be integers.");
                return NULL;
            }
            coordinate[i] = PyLong_AS_LONG(temp);
        }
        if(coordinate[0] > self->row_num || coordinate[1] > self->col_num){
            PyErr_SetString(PyExc_IndexError, "You have gone beyond the array!");
            return NULL;
        }
        result = Py_BuildValue("i", self->mtrx[coordinate[0]][coordinate[1]]);
        return result;
    }
    return NULL;
}

static Matrix *
initialize(Matrix * self, Matrix * mtrx1, Matrix * mtrx2, int operation) 
{

    PyTypeObject * type = (PyTypeObject*)PyObject_Type((PyObject*)mtrx1);
    self = (Matrix *) type->tp_alloc(type, 0);
    self->col_num = 0;
    self->mtrx = (long**)malloc(mtrx1->row_num * sizeof(long*));

    long number;

    switch (operation)
    {
    case ADD:

        for(int i = 0; i < mtrx1->row_num; i++){

            self->mtrx[i] = (long*)malloc(mtrx1->col_num * sizeof(long));

            for (int j = 0; j < mtrx1->col_num; j++) {
                
                self->mtrx[i][j] = mtrx1->mtrx[i][j] + mtrx2->mtrx[i][j];
                
            }

        }
        self->col_num = mtrx1->col_num;
        self->row_num = mtrx1->row_num;
        break;
    
    long res_mult = 0;
    case MULTIPLY:


        for(int i = 0; i < mtrx1->row_num; i++) {

            self->mtrx[i] = (long*)malloc(mtrx2->col_num * sizeof(long));

            for (int j = 0; j < mtrx2->col_num; j++) {
                
                for (int k = 0; k < mtrx1->col_num; ++k) {
                    res_mult = res_mult + mtrx1->mtrx[i][k]*mtrx2->mtrx[k][j];
                    
                }
                self->mtrx[i][j] = res_mult;
                res_mult = 0;
            }

        }

        self->col_num = mtrx2->col_num;
        self->row_num = mtrx1->row_num;
        break;
    
     
    case NUM_MULTIPLY:
        number = PyLong_AS_LONG((PyObject *)mtrx2);

        for(int i = 0; i < mtrx1->row_num; i++){

            self->mtrx[i] = (long*)malloc(mtrx1->col_num * sizeof(long));

            for (int j = 0; j < mtrx1->col_num; j++) {
                
                self->mtrx[i][j] = mtrx1->mtrx[i][j] * number;
                
            }
        }

        self->row_num = mtrx1->row_num;
        self->col_num = mtrx1->col_num;
        break;
    case DIVIDE:
        number = PyLong_AS_LONG((PyObject *)mtrx2);

        for(int i = 0; i < mtrx1->row_num; i++){

            self->mtrx[i] = (long*)malloc(mtrx1->col_num * sizeof(long));

            for (int j = 0; j < mtrx1->col_num; j++) {
                
                self->mtrx[i][j] = mtrx1->mtrx[i][j] / number;
                
            }
        }

        self->row_num = mtrx1->row_num;
        self->col_num = mtrx1->col_num;

        break;
    }

    return self;
}

static PyObject *
Matrix_add(Matrix *mtrx1, Matrix *mtrx2) 
{
    Matrix *self;

    if(mtrx1->col_num != mtrx2->col_num || mtrx1->row_num != mtrx2->row_num){
        PyErr_SetString(PyExc_ValueError, "Matrices should be equal!");
        return NULL;
    }
    
    self = initialize(self, mtrx1, mtrx2, ADD);

    return (PyObject *) self;
}


static PyObject *
Matrix_mult(Matrix *mtrx1, Matrix *mtrx2) 
{
    Matrix *self;

    if(PyLong_Check((PyObject*) mtrx1)) {
        
        self = initialize(self, mtrx2, mtrx1, NUM_MULTIPLY);
        return (PyObject *)self;

    }

    if(PyLong_Check((PyObject*) mtrx2)) {

        self = initialize(self, mtrx1, mtrx2, NUM_MULTIPLY);
        return (PyObject *)self;

    }

    if(mtrx1->col_num != mtrx2->row_num){
        PyErr_SetString(PyExc_ValueError, "First cols should equal second rows!");
        return NULL;
    }
    
    self = initialize(self, mtrx1, mtrx2, MULTIPLY);

    return (PyObject *) self;
}

static PyObject * 
Matrix_divide(Matrix * mtrx1, Matrix * mtrx2)
{
    Matrix * self;
    
    if (!PyLong_Check((PyObject*) mtrx2)) {
        PyErr_SetString(PyExc_ValueError, "You should divide by the int or long!");
        return NULL;
    }

    self = initialize(self, mtrx1, mtrx2, DIVIDE);
    return (PyObject *) self;
}

static PyObject * 
Matrix_transpos(Matrix * mtrx1)
{
    Matrix * self;

    PyTypeObject * type = (PyTypeObject*)PyObject_Type((PyObject*)mtrx1);
    self = (Matrix *) type->tp_alloc(type, 0);
    self->col_num = 0;
    self->mtrx = (long**)malloc(mtrx1->col_num * sizeof(long*));
    for(int i = 0; i < mtrx1->col_num; i++){

        self->mtrx[i] = (long*)malloc(mtrx1->row_num * sizeof(long));

        for (int j = 0; j < mtrx1->row_num; j++) {
            
            self->mtrx[i][j] = mtrx1->mtrx[j][i];
            
        }

    }

    self->col_num = mtrx1->row_num;
    self->row_num = mtrx1->col_num;

    return (PyObject *) self;
}

static int
Matrix_contains(Matrix *self, PyObject *arg)
{
    if (!PyLong_Check(arg)) {
        PyErr_SetString(PyExc_TypeError, "Object must be number!");
        return -1;
    }

    long num = PyLong_AsLong(arg);

    for(int i = 0; i < self->row_num; i++){

        for (int j = 0; j < self->col_num; j++) {
            
            if (self->mtrx[i][j] == num)
                return 1;
        }

    }
    
    return 0;
}

static PyMethodDef Matrix_methods[] = {
    {"transpos", (PyCFunction) Matrix_transpos, METH_NOARGS,
     "Return transosal matrix"
    },
    {NULL}  /* Sentinel */
};

static PyMappingMethods Matrix_as_mapping = {
    (lenfunc)0,                    /* mp_length */
    (binaryfunc)Matrix_subscript, /* mp_subscript */
    (objobjargproc)0,            /* mp_ass_subscript */
};

static PySequenceMethods Matrix_as_sequence = {
    .sq_contains = (objobjproc)Matrix_contains,
};

static PyNumberMethods Matrix_as_number = {
    .nb_add = (binaryfunc)Matrix_add,
    .nb_multiply = (binaryfunc)Matrix_mult,
    .nb_remainder = (binaryfunc)Matrix_divide,
};

static PyTypeObject MatrixType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name = "Matrix.Matrix",
    .tp_doc = "Matrix class",
    .tp_basicsize = sizeof(Matrix),
    .tp_itemsize = 0,
    .tp_flags = Py_TPFLAGS_DEFAULT,
    .tp_init = (initproc)Matrix_init,
    .tp_new = Matrix_new,
    .tp_methods = Matrix_methods,
    .tp_as_mapping = &Matrix_as_mapping,
    .tp_as_number = &Matrix_as_number,
    .tp_as_sequence = &Matrix_as_sequence,
    .tp_repr = (reprfunc)Matrix_repr,
    .tp_str = (reprfunc)Matrix_str,
};

static PyModuleDef matrixmodule = {
    PyModuleDef_HEAD_INIT,
    .m_name = "Matrix",
    .m_doc = "Example module that creates an extension type.",
    .m_size = -1,
};

PyMODINIT_FUNC
PyInit_Matrix(void)
{
    PyObject *m;
    if (PyType_Ready(&MatrixType) < 0)
        return NULL;

    m = PyModule_Create(&matrixmodule);
    if (m == NULL)
        return NULL;

    Py_INCREF(&MatrixType);
    PyModule_AddObject(m, "Matrix", (PyObject *) &MatrixType);
    return m;
}