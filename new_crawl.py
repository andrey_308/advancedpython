import asyncio
import aiohttp
import logging
import sys
from urllib.parse import urljoin, urlparse
from lxml import html as lh

logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=logging.INFO,
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)
logger = logging.getLogger("Crawler")

seen_urls = list()

async def parser(url, session, base_url):
    found_urls = set()

    async with session.get(url) as response:
        html = await response.text()

        dom = lh.fromstring(html)
        for href in dom.xpath('//a/@href'):
            url = urljoin(base_url, href)
            if url.startswith(base_url) and url not in seen_urls:
                found_urls.add(url)
    
    found_urls = list(found_urls)
    seen_urls.extend(found_urls)
    return found_urls

        

async def worker(w_num, queue, session, base_url, depth):

    logger.info(f"Starting worker {w_num}")

    while depth > 0:
        url = await queue.get()
        logger.info(f"{w_num} worker get url: {url}")

        found_urls = await parser(url, session, base_url)
        logger.info(f"Worker {w_num} get {len(found_urls)} urls, depth: {depth}")
        for link in found_urls:
            await queue.put(link)
        depth -= 1

async def crawler(root_url, base_url, w_count, depth):

    async with aiohttp.ClientSession() as session:

        q = asyncio.Queue()
        await q.put(root_url)

        tasks = [worker(i, q, session, base_url, depth) for i in range(1, w_count+1)]
        await asyncio.gather(*tasks)
    logger.info(f"Crawler found {len(set(seen_urls))} urls")
        

if __name__ == "__main__":
    root_url = 'https://www.pichshop.ru/catalog/gift/'
    base_url = '{}://{}'.format(urlparse(root_url).scheme, urlparse(root_url).netloc)
    asyncio.run(crawler(root_url, base_url, w_count=10, depth=1))
