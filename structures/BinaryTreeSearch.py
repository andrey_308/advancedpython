class BinaryTreeNode:

    def __init__(self, value):
        self.value= value
        self._left_node = None
        self._right_node = None

class BinaryTreeSearch:

    def __init__(self, value):
        self._root = None
        self._count_nodes = 0
        if type(value) == list:
            for i in value:
                self.add(i)
        else:
            self.add(value)

    def add(self, value):
        if self._root == None:
            self._root = BinaryTreeNode(value)
            self._count_nodes += 1
        else:
            self.add_to(self._root, value)

    def add_to(self, node, value):
        if value < node.value:
            if node._left_node == None:
                node._left_node = BinaryTreeNode(value)
                self._count_nodes += 1
            else:
                self.add_to(node._left_node, value)
        else:
            if node._right_node == None:
                node._right_node = BinaryTreeNode(value)
                self._count_nodes += 1
            else:
                self.add_to(node._right_node, value)

    def contains(self, value):
        cur_node = self._root
        while cur_node != None:
            if value < cur_node.value:
                cur_node = cur_node._left_node
            elif value > cur_node.value:
                cur_node = cur_node._right_node
            else:
                break

        return cur_node != None