class LinkedListNode:

    def __init__(self, value, next=None):
        self.value = value
        self.next = next

class LinkedList:

    def __init__(self, value):
        self._head = None
        self._tail = None
        self.length = 0
        if type(value) == list:
            self.length = len(value)
            node = None
            for i in range(len(value)):
                if i == 0:
                    self._head = LinkedListNode(value[i])
                    node = self._head
                else:
                    node.next = LinkedListNode(value[i])
                    node = node.next
            self._tail = node
        else:
            self.length = 1
            self._head = LinkedListNode(value)
            self._tail = self._head

    def add(self, value): 
        if type(value) == list:
            raise ValueError('For list use extend!')
        node = LinkedListNode(value)
        self._tail.next = node
        self._tail = node
        self.length += 1
    
    def remove(self, value): 
        node = self._head
        prev_node = None

        if self._head == self._tail and self._head.value == value:
            self._head = self._tail = None
            self.length -= 1
            return True
        if self._head.value == value:
            self._head = self._head.next
            self.length -= 1
            return True
        
        while node != None:
            if node.value == value:
                prev_node.next = node.next
                if node.next == None:
                    self._tail = prev_node
                self.length -= 1
                return True
            prev_node = node
            node = node.next
        return False
        
    def clear(self):
        self._head = None
        self._tail = None
        self.length = 0

    def contains(self, value):
        node = self._head
        if node.next == None and node.value != value:
            return False
        while node != None:
            if node.value == value:
                return True
            node = node.next
        return False

    def __iter__(self):
        node = self._head
        while node != None:
            yield node.value
            node = node.next

# a = LinkedList([1,2,3])
# print(a.length)

# a.add(5)
# print(a.length)
# print(a._tail.value)

# print(a.contains(5))
# print(a.contains(2))
# for i in a:
#     print(i)

# print(a.remove(1))
# print(a.remove(6))
# print(a.remove(5))
# print(a.remove(2))
# print(a.remove(3))

# print(a._head)
