from LinkedList import LinkedList

class Stack:
    '''This stack realisation is depends on LinkedList realisation.
    All methods should have O(1) if LinkedList is double linked.
    My Stack relisation is depends on singly LinkedList, so method
    pop() have O(n)'''

    def __init__(self, value):
        self._top = LinkedList(value)

    def push(self, value):
        self._top.add(value)

    def pop(self):
        if self._top.length == 0:
            return False
        
        res = self._top._tail.value
        self._top.remove(res)

        return res

    def peek(self):
        if self._top.length == 0:
            return False
        return self._top._tail.value

    def count(self):
        return self._top.length
