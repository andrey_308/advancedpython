import aiohttp
import asyncio
from asyncio import Queue
import logging
from bs4 import BeautifulSoup
from aioelasticsearch import Elasticsearch
from urllib.parse import urljoin, urlparse
from lxml import html as lh
from time import time

start_url = 'http://mathprofi.ru/'
base_url = '{}://{}'.format(urlparse(start_url).scheme, urlparse(start_url).netloc)


class Crawler:

    def __init__(self, url, depth, rps):
        self.s_url = url
        self.depth = depth - 1
        self.rps = rps
        self.queue = Queue()
        self.timeout = aiohttp.ClientTimeout(total=60*15)
        self.seen_urls = []
        self.id_counter = 0
        self.es = None
        asyncio.run(self.main())
        print(len(self.seen_urls))

    async def fetch(self, session, url):
        html = ""
        async with session.get(url, timeout=self.timeout) as response:
            resp_status = response.status
            if resp_status == 404:
                print("Got [{}] for URL: {}".format(resp_status, url))
                return None
            # logger.info("Got response [%s] for URL: %s", resp_status, url)
            try:
                html = await response.text()
            except UnicodeDecodeError:
                print("UnicodeDecodeError detected for URL: {}".format(url))
                return None
            return html

    async def parse_link(self, url, session, es):
        found_urls = set()

        try:
            html = await self.fetch(session, url)
            if not html:
                return None
        except (
            aiohttp.ClientError,
            aiohttp.http_exceptions.HttpProcessingError,
        ) as e:
            print(
                "aiohttp exception for {} [{}]: {}".format(
                url,
                getattr(e, "status", None),
                getattr(e, "message", None),
            ))
            return None
        except Exception as e:
            print(
                "Non-aiohttp exception occured:  {}".format(getattr(e, "__dict__", {}))
            )
            return None
        else:
            soup = BeautifulSoup(html, features="lxml")
            text = soup.get_text()

            es_answer = await es.index(index=url, id=self.id_counter, doc_type="text", body=text)
            print(es_answer['_id'])
            #TODO Putting into Elasticsearch

            dom = lh.fromstring(html)
            for href in dom.xpath('//a/@href'):
                url = urljoin(base_url, href)
                if url.startswith(base_url):
                    found_urls.add(url)
            return list(found_urls)


    async def add_urls(self, session, url, es):
        res = await self.parse_link(url, session, es)

        if not res:
            return None
        try:
            res = sum(res, [])
        except TypeError:
            pass
        
        self.seen_urls.extend(res)

        if self.depth > 0:
            await self.queue.put(res)


    async def crawl(self, session):
        es = self.es
        while not self.queue.empty():
            urls = await self.queue.get()
            if type(urls) == set:
                urls = list(urls)
            if type(urls) == str:
                urls = [urls]
            tasks = []
            print("Found {} links".format(len(urls)))
            for url in urls:
                tasks.append(self.add_urls(session, url, es))
            await asyncio.gather(*tasks)
            self.depth -= 1

    async def main(self):
        
        async with aiohttp.ClientSession(timeout=self.timeout) as session:
            self.es = Elasticsearch()
            print("new session")
            await self.queue.put(self.s_url)
            await self.crawl(session)


strt = time()
crawl = Crawler(url=start_url, depth=2, rps=80)
end = time()
print(end-strt)