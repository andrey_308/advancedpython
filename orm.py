import MySQLdb
from MySQLdb import OperationalError
import gc

db = MySQLdb.connect(
                     host="localhost",
                     user="root",
                     password="1234",
                     db="User")

cursor = db.cursor()

class Field:
    def __init__(self, f_type, sql_type, required=True, default=None):
        self.f_type = f_type
        self.sql_type = sql_type
        self.required = required
        self.default = default

    def validate(self, value):
        if value is None or not self.required:
            return None
        
        return self.f_type(value)

class IntField(Field):
    def __init__(self, required=True, default=None):
        super().__init__(int, 'INT', required, default)

class StringField(Field):
    def __init__(self, required=True, default=None):
        super().__init__(str, 'VARCHAR', required, default)

class ModelMeta(type):
    def __new__(mcs, name, bases, namespace):
        if name == 'Model':
            return super().__new__(mcs, name, bases, namespace)

        meta = namespace.get('Meta')
        if meta is None:
            raise ValueError('meta is none')
        if not hasattr(meta, 'table_name'):
            raise ValueError('table_name is empty')

        fields = {k: v for k, v in namespace.items()
                  if isinstance(v, Field)}
        base_fields = {}
        for base in bases:
            if base.__name__ != 'Model':
                base_fields = {k: v for k, v in base.__dict__['_fields'].items()
                          if isinstance(v, Field)}

        fields.update(base_fields)
        namespace['_fields'] = fields
        namespace['_table_name'] = meta.table_name
        return super().__new__(mcs, name, bases, namespace)

class Manage:
    def __init__(self):
        self.model_cls = None

    def __get__(self, instance, owner):
        self._where = ""
        self._request = ""
        self.instance = instance
        if self.model_cls is None or self.model_cls != owner:
            self.model_cls = owner
        return self

    def create(self, **kwargs):
        self.save(kwargs)

    def update(self, **kwargs):
        self._kwargs = kwargs
        self._request = "UPDATE {}".format(self.model_cls._table_name)
        self._request += " SET "
        self._request += self._fltr(kwargs, False)
        return self

    def save(self, f_args=0):
        if self.instance == None:
            source = self.model_cls
        else:
            source = self.instance
        if f_args == 0:
            fields = list(i for i in source.__dict__.keys()
                          if i in source._fields.keys())
        else:
            fields = list(i for i in f_args.keys()
                          if i in source._fields.keys())
        sql = "INSERT INTO {}".format(source._table_name)
        sql += "("
        for i in fields:
            sql += "{}, ".format(i)
        sql = sql[:-2] + ") values ("
        if f_args == 0:
            for j in list(source.__dict__.values()):
                sql += "'{}', ".format(j)
        else:
            for j in list(f_args.values()):
                sql += "'{}', ".format(j)
        sql = sql[:-2] + ")"
        cursor.execute(sql)
        db.commit()

    def _fltr(self, a, check):
        val = list(a.items())
        if check:
            bunch = " and "
        else:
            bunch = ", "
        set_str = "{} = '{}'" + bunch
        for i in val:
            set_str = set_str.format(*i)
            set_str += "{} = '{}'" + bunch
        if check:
            set_str = set_str[:-19]
        else:
            set_str = set_str[:-13]
        return set_str

    def where(self, **kwargs):
        self._where += " WHERE "
        self._where += self._fltr(kwargs,True)
        return self

    def delete(self, **kwargs):
        table = self.model_cls._table_name
        self._request = "DELETE FROM "
        self._request += "{}".format(table)
        return self
        
    def execute(self):
        _sql = self._request + self._where
        cursor.execute(_sql)
        if "SELECT" in _sql:
            data = cursor.fetchall()
            print(data)
        else:
            db.commit()
    
    def get(self):
        table = self.model_cls._table_name
        self._request = "SELECT * FROM "
        self._request += "{}".format(table)
        return self

class Model(metaclass=ModelMeta):
    
    objects = Manage()

    def __init__(self, *_, **kwargs):
        for field_name, field in self._fields.items():
            value = field.validate(kwargs.get(field_name))
            if value is not None:
                setattr(self, field_name, value)

    @classmethod
    def create_table(cls):
        cls_fields = [*cls._fields.keys()]
        set_sql = " ("
        for field in cls_fields:
            set_sql = set_sql + field + " "
            set_sql += cls._fields[field].sql_type
            set_sql += "(20), "
        set_sql = set_sql[:-2]
        set_sql += ")"
        sql = "CREATE TABLE " + \
            cls._table_name + \
            set_sql
        try:
            cursor.execute(sql)
            db.commit()
        except OperationalError:
            print("Table is exist!") 


"""Далее идет пример использования orm"""

# class User(Model):
#     id = IntField()
#     nic = StringField()

#     class Meta:
#         table_name = 'Users'

# User.create_table()

# class Man(User):
#     name = StringField()
#     sex = StringField()
#     age = IntField()

#     class Meta:
#         table_name = 'Man'

# Man.create_table()

# class Alex(Man):
#     pet = StringField()

#     class Meta:
#         table_name = 'Alex'

# Alex.create_table()

# user = User(id=1, nic='name')
# user.id = '2'
# user.objects.save()

# alex = Man(nic='DaD', name='Alex', sex='male', age=19)
# alex.objects.save()

# User.objects.update(nic='DaD').where(id=1).execute()

# User.objects.update(nic='Bobby').where(id=2).execute()

# Man.objects.get().where(nic='DaD').execute()

# Alex.objects.create(id=3, pet='Cat')

# Man.objects.update(sex='female').where(name='Alex').execute()
# Man.objects.delete().where(name='Alex').execute()

db.close()
gc.collect()