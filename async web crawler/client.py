import requests as r
import json

z = r.post("http://localhost:8080/signup", data={'email': 'b@.ru', 'password': '123', 'name': 'bob'})
print(z.text)
z = r.post("http://localhost:8080/login", data={'email': 'b@.ru', 'password': '123'})
print(z.text)
a = json.loads(z.text)
token = a['data']['token']
z = r.get("http://localhost:8080/current", headers={'X-Token': str(token)})
print(z.text)
z = r.post("http://localhost:8080/index", headers={'X-Token': str(token)},
           data={'domain': 'https://ru.wikibooks.org/wiki/'})
print(z.text)
z = r.get("http://localhost:8080/stat", headers={'X-Token': str(token)})
print(z.text)
z = r.get("http://localhost:8080/search", params={'q': 'высшая', 'limit': 5, 'offset': 2})
print(z.text)
