import gc
import logging
import sys

import aiomysql
from aiomysql import InternalError

logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=logging.INFO,
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)
logger = logging.getLogger("ORM")


class Field:
    def __init__(self, f_type, sql_type, required=True, default=None):
        self.f_type = f_type
        self.sql_type = sql_type
        self.required = required
        self.default = default

    def validate(self, value):
        if value is None or not self.required:
            return None
        # print(value)
        return self.f_type(value)


class IntField(Field):
    def __init__(self, required=True, default=None):
        super().__init__(int, 'INT', required, default)


class StringField(Field):
    def __init__(self, required=True, default=None):
        super().__init__(str, 'TEXT', required, default)


class FloatField(Field):
    def __init__(self, required=True, default=None):
        super().__init__(float, 'FLOAT', required, default)


class DateTimeField(Field):
    def __init__(self, required=True, default=None):
        super().__init__(str, 'DATETIME', required, default)


class ModelMeta(type):
    def __new__(mcs, name, bases, namespace):
        if name == 'Model':
            return super().__new__(mcs, name, bases, namespace)

        meta = namespace.get('Meta')
        if meta is None:
            raise ValueError('meta is none')
        if not hasattr(meta, 'table_name'):
            raise ValueError('table_name is empty')

        fields = {k: v for k, v in namespace.items()
                  if isinstance(v, Field)}
        base_fields = {}
        for base in bases:
            if base.__name__ != 'Model':
                base_fields = {k: v for k, v in base.__dict__['_fields'].items()
                               if isinstance(v, Field)}

        fields.update(base_fields)
        namespace['_fields'] = fields
        namespace['_table_name'] = meta.table_name
        return super().__new__(mcs, name, bases, namespace)


class Manage:
    def __init__(self, loop=None):
        self.model_cls = None
        self.db = None
        self.cursor = None

    async def connect(self, loop):
        db = await aiomysql.connect(
            host="localhost",
            user="root",
            password="1234",
            db="Crawler",
            loop=loop)
        self.db = db
        self.cursor = await self.db.cursor()
        return self.db

    def __get__(self, instance, owner):
        self._where = ""
        self._request = ""
        self.instance = instance
        if self.model_cls is None or self.model_cls != owner:
            self.model_cls = owner
        return self

    async def create(self, **kwargs):
        await self.save(kwargs)

    def update(self, **kwargs):
        self._kwargs = kwargs
        self._request = "UPDATE {}".format(self.model_cls._table_name)
        self._request += " SET "
        self._request += self._fltr(kwargs, False)
        return self

    async def save(self, f_args=0):
        if self.instance == None:
            source = self.model_cls
        else:
            source = self.instance
        if f_args == 0:
            fields = list(i for i in source.__dict__.keys()
                          if i in source._fields.keys())
        else:
            fields = list(i for i in f_args.keys()
                          if i in source._fields.keys())
        sql = "INSERT INTO {}".format(source._table_name)
        sql += "("
        for i in fields:
            sql += "{}, ".format(i)
        sql = sql[:-2] + ") values ("
        if f_args == 0:
            for j in list(source.__dict__.values()):
                sql += "'{}', ".format(j)
        else:
            for j in list(f_args.values()):
                sql += "'{}', ".format(j)
        sql = sql[:-2] + ")"
        await self.cursor.execute(sql)
        await self.db.commit()

    def _fltr(self, a, check):
        val = list(a.items())
        if check:
            bunch = " and "
        else:
            bunch = ", "
        set_str = "{} = '{}'" + bunch
        for i in val:
            set_str = set_str.format(*i)
            set_str += "{} = '{}'" + bunch
        if check:
            set_str = set_str[:-19]
        else:
            set_str = set_str[:-13]
        return set_str

    def where(self, **kwargs):
        self._where += " WHERE "
        self._where += self._fltr(kwargs, True)
        return self

    def delete(self, **kwargs):
        table = self.model_cls._table_name
        self._request = "DELETE FROM "
        self._request += "{}".format(table)
        return self

    async def execute(self):
        _sql = self._request + self._where
        # print(_sql)
        await self.cursor.execute(_sql)
        if "SELECT" in _sql:
            data = await self.cursor.fetchall()
            return data
        else:
            await self.db.commit()

    def get(self, *args):
        table = self.model_cls._table_name
        self._request = "SELECT "
        if not args:
            self._request += "*"
        else:
            for field in args:
                self._request += field
                self._request += ", "
            self._request = self._request[:-2]
        self._request += " FROM "
        self._request += "{}".format(table)
        # print(self._request)
        return self


class Model(metaclass=ModelMeta):
    objects = Manage()

    @classmethod
    async def create_conn(cls, loop):
        return await cls.objects.connect(loop)

    def __init__(self, **kwargs):
        for field_name, field in self._fields.items():
            value = field.validate(kwargs.get(field_name))
            if value is not None:
                setattr(self, field_name, value)

    @classmethod
    async def fetch(cls, sql):
        await cls.objects.cursor.execute(sql)
        await cls.objects.db.commit()

    @classmethod
    async def create_table(cls):
        cls_fields = [*cls._fields.keys()]
        set_sql = " ("
        for field in cls_fields:
            set_sql = set_sql + field + " "
            set_sql += cls._fields[field].sql_type
            if cls._fields[field].sql_type == "DATETIME":
                set_sql += "(6)"
            else:
                set_sql += "(20)"
            if field == "id":
                set_sql += "AUTO_INCREMENT, PRIMARY KEY (id)"
            set_sql += ", "
        set_sql = set_sql[:-2]
        set_sql += ")"
        sql = "CREATE TABLE " + \
              cls._table_name + \
              set_sql
        # print(sql)
        try:
            await cls.fetch(sql)
        except InternalError:
            print("Table is exist!")


gc.collect()
