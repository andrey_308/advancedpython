import asyncio
import aioamqp
import async_orm
from random import randint
from datetime import datetime
from time import time
import logging
import sys
import json

logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=logging.INFO,
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)
logger = logging.getLogger("AUTH")


class User(async_orm.Model):
    id = async_orm.IntField()
    email = async_orm.StringField()
    password = async_orm.StringField()
    name = async_orm.StringField()
    num_urls = async_orm.IntField()
    created_date = async_orm.DateTimeField()
    last_login_date = async_orm.DateTimeField()

    class Meta:
        table_name = "User"


class Token(async_orm.Model):
    token = async_orm.StringField()
    user_id = async_orm.IntField()
    expire_date = async_orm.DateTimeField()

    class Meta:
        table_name = "Token"


def generate_token(tokens):
    token = None
    while not token:
        rand_token = randint(0, 1000000000)
        token = rand_token if rand_token not in tokens else None
    return token


async def signup(task_id, email, passw, name):
    user_exsist = await User.objects.get().where(email=email).execute()
    if user_exsist:
        return {'task_id': task_id}
    user = User(email=email, password=passw, name=name, num_urls=0,
                created_date=datetime.fromtimestamp(time()).strftime('%Y-%m-%d %H:%M:%S'),
                last_login_date=datetime.fromtimestamp(time()).strftime('%Y-%m-%d %H:%M:%S'))
    await user.objects.save()
    id = await User.objects.get("id").where(email=email).execute()
    data = await Token.objects.get("token").execute()
    tokens = data[0]
    token = generate_token(tokens)
    print(f"TOKEN: {token}")
    expire = datetime.fromtimestamp(time() + 3600 * 2).strftime('%Y-%m-%d %H:%M:%S')
    user_token = Token(token=token, user_id=id[0][0], expire_date=expire)
    await user_token.objects.save()
    return {'task_id': task_id, 'token': token, 'expire': expire}


async def login(task_id, email, passw):
    user_passw = await User.objects.get("password").where(email=email).execute()
    if user_passw:
        if passw == user_passw[0][0]:
            await User.objects.update(
                last_login_date=datetime.fromtimestamp(time()).strftime('%Y-%m-%d %H:%M:%S')).where(
                email=email).execute()
            id = await User.objects.get("id").where(email=email).execute()
            id = id[0][0]
            token_data = await Token.objects.get("token", "expire_date").where(user_id=id).execute()
            if not token_data:
                return {'task_id': task_id, 'message': "Please sign up!"}
            token = token_data[0][0]
            expire_date = token_data[0][1]
            active_token = True if expire_date > datetime.now() else False
            data = await Token.objects.get("token").execute()
            tokens = data[0]
            if not active_token:
                logger.info(f"Previous token ({token}) is unactive: expire time:{expire_date} > now:{datetime.now()}")
                token = generate_token(tokens)
                expire = datetime.fromtimestamp(time() + 3600 * 2).strftime('%Y-%m-%d %H:%M:%S')
                await Token.objects.update(token=token, expire_date=expire).where(user_id=id).execute()
                logger.info(f"Create new token: {token}")
                return {'task_id': task_id, 'token': token, 'expire': expire}
            else:
                return {'task_id': task_id, 'token': token, 'expire': expire_date}
        else:
            return {'task_id': task_id, 'message': "Wrong password!"}
    else:
        return {'task_id': task_id, 'message': "User doesn't exist!"}


async def validate(task_id, token):
    token_data = await Token.objects.get("user_id", "expire_date").where(token=token).execute()
    user_id = token_data[0][0]
    expire_date = token_data[0][1]
    check_date = True if expire_date > datetime.now() else False
    if not check_date:
        return {'task_id': task_id}
    user_data = await User.objects.get().where(id=user_id).execute()
    res = {'task_id': task_id, 'id': user_data[0][0], 'email': user_data[0][1], 'password': user_data[0][2],
           'name': user_data[0][3], 'num_urls': user_data[0][4], 'created_date': user_data[0][5],
           'last_login_date': user_data[0][6]}
    return res


async def main(request):
    method = request['method']

    if method == "signup":
        task_id = request['task_id']
        message = json.loads(request['message'])
        email = message['email']
        password = message['password']
        name = message['name']
        return await signup(task_id, email, password, name)

    if method == "login":
        print(request)
        task_id = request['task_id']
        message = json.loads(request['message'])
        email = message['email']
        password = message['password']
        return await login(task_id, email, password)

    if method == "validate":
        message = json.loads(request['message'])
        token = message['token']
        task_id = request['task_id']
        return await validate(task_id, token)


async def callback_inbound(channel, body, envelope, properties):
    body = json.loads(body)
    print(" [x] validate Received %r from inbound" % body)
    logger.info(f"Get {body}")

    message = await main(body)

    transport, protocol = await aioamqp.connect()
    channel_out = await protocol.channel()

    await channel_out.queue(queue_name='outbound', durable=True)
    message = json.dumps(message, default=str)
    await channel_out.basic_publish(payload=message, exchange_name='', routing_key='outbound',
                                    properties={'delivery_mode': 2, }, )
    logger.info(f"Publish {message} to outbound")
    await channel.basic_client_ack(delivery_tag=envelope.delivery_tag)


async def run(loop):
    await async_orm.Model.create_conn(loop)
    await Token.create_table()
    await User.create_table()
    transport, protocol = await aioamqp.connect()
    channel = await protocol.channel()
    await channel.queue('inbound', durable=True)
    await channel.basic_consume(queue_name='inbound', callback=callback_inbound)

    try:
        await asyncio.Future()
    except:
        pass
    await protocol.close()
    transport.close()


loop = asyncio.get_event_loop()
loop.run_until_complete(run(loop))
