import asyncio
from aiohttp import web
import aioamqp
import logging
import sys
from random import randint
import async_orm
from aioelasticsearch import Elasticsearch
import datetime
import json

logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=logging.INFO,
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)
logger = logging.getLogger("API")


class CrawlStats(async_orm.Model):
    domain = async_orm.StringField()
    author_id = async_orm.IntField()
    https = async_orm.IntField()
    time = async_orm.DateTimeField()
    pages_count = async_orm.IntField()
    avg_time_per_page = async_orm.FloatField()
    max_time_per_page = async_orm.FloatField()
    min_time_per_page = async_orm.FloatField()

    class Meta:
        table_name = "CrawlStats"


class WebApp:
    def __init__(self):
        self.futures = {}

    async def search(self, request):
        data = request.query
        try:
            q = data['q']
            limit = data['limit']
            offset = data['offset']
        except KeyError:
            return web.Response(status=400, body="{'status': 'bad request', 'data': {}}")

        es = Elasticsearch()
        urls = []
        results = await es.search(index="urls", from_=offset, size=limit, _source="url",
                                  body={"query": {"match": {"text_of_link": f"{q}"}}})
        for row in results['hits']['hits']:
            urls.append(row['_source']['url'])
        es.transport.close()
        return web.Response(status=200, body=json.dumps({'status': 'ok', 'data': urls}))

    async def login(self, request):
        data = await request.post()
        method = 'login'
        fut = asyncio.Future()
        try:
            email = data['email']
            password = data['password']
        except KeyError:
            return web.Response(status=400, body="{'status': 'bad request', 'data': {}}")

        resp = {'email': email, 'password': password}
        resp = json.dumps(resp)

        task_id = self.add_future(fut)
        logger.info(f"Write {task_id}: {fut} to futures")

        await self.send_to_inbound(task_id, resp, method)

        logger.info(f"Wait for tasks from rabbit on {method}")
        data = await fut

        try:
            body = {'status': 'ok',
                    'data': {
                        'token': data['token'],
                        'expire': data['expire']
                    }}
        except KeyError:
            body = {'status': 'forbidden',
                    'data': {
                        'message': data['message']
                    }}
            return web.Response(status=400, body=json.dumps(body))

        return web.Response(status=200, body=json.dumps(body))

    async def signup(self, request):
        data = await request.post()
        method = 'signup'
        fut = asyncio.Future()
        try:
            email = data['email']
            password = data['password']
            name = data['name']
        except KeyError:
            return web.Response(status=400, body="{'status': 'bad request', 'data': {}}")

        resp = {'email': email, 'password': password, 'name': name}
        resp = json.dumps(resp)

        task_id = self.add_future(fut)
        logger.info(f"Write {task_id}: {fut} to futures")

        await self.send_to_inbound(task_id, resp, method)

        logger.info(f"Wait for tasks from rabbit on {method}")
        data = await fut

        try:
            body = {'status': 'ok',
                    'data': {
                        'token': data['token'],
                        'expire': data['expire']
                    }}
        except KeyError:
            return web.Response(status=400, body=json.dumps({'status': 'user with this email is already exist!'}))

        return web.Response(status=200, body=json.dumps(body))

    async def send_to_inbound(self, task_id, message, method):
        transport, protocol = await aioamqp.connect()
        channel = await protocol.channel()
        await channel.queue_declare(queue_name='inbound', durable=True)
        message = json.dumps({'task_id': task_id, 'message': message, 'method': method})
        await channel.basic_publish(payload=message, exchange_name='', routing_key='inbound',
                                    properties={'delivery_mode': 2, }, )
        logger.info(f"Publish task_id:{task_id}, token:{message}, method: {method} to inbound")

    async def send_to_index(self, domain, task_id, user_id, num_urls):
        transport, protocol = await aioamqp.connect()
        channel = await protocol.channel()
        await channel.queue_declare(queue_name='index', durable=True)
        message = json.dumps({'task_id': task_id, 'domain': domain, 'user_id': user_id, 'num_urls': num_urls})
        await channel.basic_publish(payload=message, exchange_name='', routing_key='index',
                                    properties={'delivery_mode': 2, }, )
        logger.info(f"Publish tmessage:{message} to index")

    def add_future(self, future):
        task_id = None
        while task_id is None:
            rand_id = randint(0, 1000000)
            task_id = rand_id if rand_id not in self.futures.keys() else None
        self.futures.update({task_id: future})
        return task_id

    async def current(self, request):
        # TODO check token (instanse and expire time) (VALIDATE)
        token = request.headers['X-Token']
        method = "current"
        fut = asyncio.Future()
        task_id = self.add_future(fut)
        logger.info(f"Write {task_id}: {fut} to futures")
        await self.send_to_inbound(task_id, json.dumps({'token': token}),
                                   'validate')  # go to auth throw rabbit mq and then to User.db
        logger.info(f"Wait for tasks from rabbit on {method}")
        data = await fut
        try:
            body = {'status': 'ok',
                    'data': {'id': data['id'],
                             'email': data['email'],
                             'name': data['name'],
                             'created_date': data['created_date'],
                             'last_login_date': data['last_login_date']}}
        except KeyError:
            return web.Response(status=403, body=json.dumps({'status': 'forbidden', 'data': {}}))
        return web.Response(status=200,
                            body=(json.dumps(body)))

    async def index(self, request):
        data = await request.post()
        print(data.keys())
        fut = asyncio.Future()
        try:
            domain = data['domain']
            token = request.headers['X-Token']
        except KeyError:
            return web.Response(status=400, body=(json.dumps({'status': 'bad request', 'data': {}})))

        resp = {'domain': domain, 'token': token}
        resp = json.dumps(resp)

        task_id = self.add_future(fut)
        logger.info(f"Write {task_id}: {fut} to futures")

        method = 'validate'
        await self.send_to_inbound(task_id, resp, method)

        logger.info(f"Wait for tasks from rabbit on {method}")
        data = await fut

        try:
            user_id = data['id']
            num_urls = data['num_urls']
        except KeyError:
            return web.Response(status=403, body=json.dumps({'status': 'forbidden', 'data': '{}'}))

        fut_index = asyncio.Future()
        task_id = self.add_future(fut_index)
        logger.info(f"Write {task_id}: {fut_index} to futures")
        await self.send_to_index(task_id=task_id, domain=domain, user_id=user_id, num_urls=num_urls)

        logger.info(f"Wait for tasks from outindex")
        index_data = await fut_index

        task_id = index_data['task_id']

        try:
            message = index_data['message']
            return web.Response(status=400, body=json.dumps({'status': 'bad domain', 'message': message}))
        except KeyError:
            pass

        return web.Response(status=200, body=json.dumps({'status': 'ok', 'data': {'task_id': task_id}}))

    async def stat(self, request):
        await async_orm.Model.create_conn(loop)
        token = request.headers['X-Token']
        fut = asyncio.Future()
        task_id = self.add_future(fut)
        logger.info(f"Write {task_id}: {fut} to futures")

        method = 'validate'
        await self.send_to_inbound(task_id, json.dumps({'token': token}), method)

        logger.info(f"Wait for tasks from rabbit on {method}")
        data = await fut

        try:
            user_id = data['id']
        except KeyError:
            return web.Response(status=403, body=json.dumps({'status': 'forbidden', 'data': {}}))

        stats = await CrawlStats.objects.get().where(author_id=user_id).execute()
        body = []
        for stat in stats:
            statistics = {'domain': stat[0],
                          'https': stat[2],
                          'time': stat[3],
                          'pages_count': stat[4],
                          'avg_time_per_page': stat[5],
                          'max_time_per_page': stat[6],
                          'min_time_per_page': stat[7]}
            body.append(statistics)
        return web.Response(status=200, body=json.dumps({'status': 'ok', 'data': body}, default=str))

    async def callback_outindex(self, channel, body, envelope, properties):
        body = json.loads(body)
        print(" [x] index Received %r from outindex" % body)
        task_id = body['task_id']
        future = self.futures[task_id]
        future.set_result(body)
        await channel.basic_client_ack(delivery_tag=envelope.delivery_tag)

    async def callback_outbound(self, channel, body, envelope, properties):
        body = json.loads(body)
        print(" [x] index Received %r from outbound" % body)
        task_id = body['task_id']
        future = self.futures[task_id]
        future.set_result(body)
        await channel.basic_client_ack(delivery_tag=envelope.delivery_tag)


async def init(loop):
    conn = await async_orm.Model.create_conn(loop)
    web_app = WebApp()

    transport, protocol = await aioamqp.connect()
    channel_index = await protocol.channel()
    await channel_index.queue('outindex', durable=True)
    await channel_index.basic_consume(web_app.callback_outindex, queue_name='outindex')

    transport, protocol = await aioamqp.connect()
    channel_auth = await protocol.channel()
    await channel_auth.queue('outbound', durable=True)
    await channel_auth.basic_consume(web_app.callback_outbound, queue_name='outbound')

    app = web.Application(loop=loop)
    router = app.router
    router.add_get('/search', web_app.search)
    router.add_post('/login', web_app.login)
    router.add_post('/signup', web_app.signup)
    router.add_get('/current', web_app.current)
    router.add_post('/index', web_app.index)
    router.add_get('/stat', web_app.stat)
    conn.close()
    return app


loop = asyncio.get_event_loop()
app = loop.run_until_complete(init(loop))

web.run_app(app)
loop.close()
