import aiohttp
import asyncio
import aioamqp
from asyncio import Queue
import async_orm 
import json
import logging
import json
import sys
from bs4 import BeautifulSoup
from aioelasticsearch import Elasticsearch
from urllib.parse import urljoin, urlparse
from lxml import html as lh
from time import time
from datetime import datetime, timedelta

logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=logging.INFO,
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)
logger = logging.getLogger("Crawler")


class CrawlStats(async_orm.Model):
    domain = async_orm.StringField()
    author_id = async_orm.IntField()
    https = async_orm.IntField()
    time = async_orm.DateTimeField()
    pages_count = async_orm.IntField()
    avg_time_per_page = async_orm.FloatField()
    max_time_per_page = async_orm.FloatField()
    min_time_per_page = async_orm.FloatField()

    class Meta:
        table_name = "CrawlStats"


class User(async_orm.Model):
    id = async_orm.IntField()
    email = async_orm.StringField()
    password = async_orm.StringField()
    name = async_orm.StringField()
    num_urls = async_orm.IntField()
    created_date = async_orm.DateTimeField()
    last_login_date = async_orm.DateTimeField()

    class Meta:
        table_name = "User"


class Crawler:

    def __init__(self, url, depth, rps, user_id, user_urls_num, task_id):
        self.s_url = url
        self.user_id = user_id
        self.task_id = task_id
        self.user_urls_num = user_urls_num
        self.base_url = '{}://{}'.format(urlparse(self.s_url).scheme, urlparse(self.s_url).netloc)
        self.domain = urlparse(self.s_url).netloc
        self.times = []
        self.https = True if urlparse(self.s_url).scheme == 'https' else False
        self.stats = {'domain': self.domain, 'https': self.https, 'time': datetime.fromtimestamp(time()).strftime('%Y-%m-%d %H:%M:%S')}
        self.depth = depth - 1
        self.rps = rps
        self.queue = Queue()
        self.timeout = aiohttp.ClientTimeout(total=60*15)
        self.seen_urls = [self.s_url]
        self.id_counter = 0
        self.es = None
        self.rps_time=None
        self.current_rps = self.rps

    async def fetch(self, session, url):
        html = ""
        if self.rps_time == None:
            self.rps_time = time()
        current_time = time()
        rps_time = current_time - self.rps_time
        logger.info(f"Rps time: {rps_time}, rps: {self.current_rps}")
        if self.current_rps <= 0:
            if rps_time < 1:
                logger.info(f"Sleep for {1 - rps_time} secs")
                await asyncio.sleep(1 - rps_time)
            self.current_rps = self.rps
            self.rps_time = time()
        async with session.get(url, timeout=self.timeout) as response:
            resp_status = response.status
            if resp_status == 404:
                logger.info("Got response [%s] for URL: %s", resp_status, url)
                return None
            try:
                html = await response.text()
            except UnicodeDecodeError:
                logger.warning("UnicodeDecodeError detected for URL: %s", url)
                return None
            self.current_rps -= 1
            return html

    async def parse_link(self, url, session, es):
        found_urls = set()

        try:
            html = await self.fetch(session, url)
            if not html:
                return None
        except (
            aiohttp.ClientError,
            aiohttp.http_exceptions.HttpProcessingError,
        ) as e:
            logger.error(
                "aiohttp exception for %s [%s]: %s",
                url,
                getattr(e, "status", None),
                getattr(e, "message", None),
            )
            return None
        except Exception as e:
            logger.exception(
            "Non-aiohttp exception occured:  %s", getattr(e, "__dict__", {})
            )
            return None
        else:
            soup = BeautifulSoup(html, features="lxml")
            text = soup.get_text()

            self.user_urls_num += 1
            id_to_elastic = str(self.user_id) + "_" + str(self.user_urls_num)
            es_answer = await es.index(index='urls', 
                                       id=f"{id_to_elastic}",
                                       doc_type='url_info',
                                       body={'url': f'{url}', 'text_of_link': '{}'.format(text)}) 
            # print(es_answer['_id'])

            if self.depth > 0:
                dom = lh.fromstring(html)
                for href in dom.xpath('//a/@href'):
                    url = urljoin(self.base_url, href)
                    if url.startswith(self.base_url):
                        found_urls.add(url)
                        
            return list(found_urls)


    async def add_urls(self, session, url, es):

        start_of_parse = time()
        res = await self.parse_link(url, session, es)
        end_of_parse = time()

        self.times.append(end_of_parse-start_of_parse)

        if not res:
            return None
        try:
            res = sum(res, [])
        except TypeError:
            pass
        
        self.seen_urls.extend(res)

        return res


    async def crawl(self, session):
        while not self.queue.empty():
            url = await self.queue.get()
            found_urls = await self.add_urls(session, url, self.es)
            if self.depth > 0:
                logger.info("Found %d links", len(found_urls))
                logger.info("Put {} links in queue, depth = {}".format(len(found_urls), self.depth))
                for link in found_urls:
                    await self.queue.put(link)
            self.depth -= 1

    async def create_index(self, es_object, index_name):
        settings = {
            "settings": {
                "number_of_shards": 1,
                "number_of_replicas": 0
            },
            "mappings": {
                "url_info": {
                    "url": {
                        "type": "text"
                    },
                    "text_of_link": {
                        "type": "text"
                    },
                }
            }
        }
        try:
            if not await es_object.indices.exists(index_name):
                await es_object.indices.create(index=index_name, ignore=400, body=settings)
                logger.info('Created Index')
        except Exception as ex:
            print(str(ex))

    async def main(self, channel):
        async with aiohttp.ClientSession(timeout=self.timeout) as session:
            self.es = Elasticsearch(timeout=60)
            #await self.es.indices.delete(index='url_text')
            await self.create_index(self.es, "urls")
            await self.queue.put(self.s_url)
            await self.crawl(session)
            await self.es.transport.close()
        self.stats.update({'pages_count': len(self.seen_urls), 'max_time_per_page': float(max(self.times)), 'min_time_per_page': float(min(self.times)), 'avg_time_per_page': float(sum(self.times)/len(self.seen_urls))})
        stats_inst = CrawlStats(domain=self.stats['domain'],
                                author_id=self.user_id,
                                https=self.stats['https'],
                                time=self.stats['time'],
                                pages_count=self.stats['pages_count'],
                                avg_time_per_page=self.stats['avg_time_per_page'],
                                max_time_per_page=self.stats['max_time_per_page'],
                                min_time_per_page=self.stats['min_time_per_page'])
        await stats_inst.objects.save()
        prev_num_urls = await User.objects.get("num_urls").where(id=self.user_id).execute()
        await User.objects.update(num_urls=prev_num_urls[0][0] + self.stats['pages_count']).where(id=self.user_id).execute()
        await channel.queue('outindex', durable=True)
        message = {'task_id': self.task_id}
        message = json.dumps(message)
        await channel.basic_publish(payload=message, exchange_name='', routing_key='outindex', properties={'delivery_mode': 2,},)


async def callback(channel, body, envelope, properties):
    body = json.loads(body)
    T = 3600
    print(" [y] Received %r" % body)
    url = body['domain']
    user_id = body['user_id']
    num_urls = body['num_urls']
    task_id = body['task_id']
    prev_time = await CrawlStats.objects.get("time").where(domain=urlparse(url).netloc).execute()
    try:
        prev_time = prev_time[-1][0]

        if (datetime.now() - prev_time) < timedelta(seconds=T):
            transport, protocol = await aioamqp.connect()
            channel_out = await protocol.channel()

            await channel_out.queue(queue_name='outindex', durable=True)
            print("we are here")
            message = {'task_id': task_id, 'message':'This domain is already cralwed!'}
            message = json.dumps(message)
            await channel_out.basic_publish(payload=message, exchange_name='', routing_key='outindex', properties={'delivery_mode': 2,},)
        else:
            crawl = Crawler(url=url, depth=2, rps=20, user_id=user_id, user_urls_num=num_urls, task_id=task_id)
            loop.create_task(crawl.main(channel))
    except IndexError:
        crawl = Crawler(url=url, depth=2, rps=20, user_id=user_id, user_urls_num=num_urls, task_id=task_id)
        loop.create_task(crawl.main(channel))
    
    await channel.basic_client_ack(delivery_tag=envelope.delivery_tag)

async def run(loop):
    await async_orm.Model.create_conn(loop)
    await User.create_table()
    await CrawlStats.create_table()
    transport, protocol = await aioamqp.connect()
    channel = await protocol.channel()
    await channel.queue('index', durable=True)
    await channel.basic_consume(callback, queue_name='index')
    
    try:
        await asyncio.Future()
    except:
        pass
    await protocol.close()
    transport.close()


loop = asyncio.get_event_loop()
loop.run_until_complete(run(loop))

