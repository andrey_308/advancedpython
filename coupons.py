import pyqrcode
from multiprocessing import Process, Pool, Queue, Manager
import imgkit
import vk
import requests
from time import time, sleep
from functools import partial
from random import randint
from string import Template


token = '******'

session = vk.Session(access_token=token)
api = vk.API(session, v=5.95)

user_id = 139732652

text = '''It's hard to believe that it came to this 
You paralyzed my body with a poison kiss
For 40 days and nights'''

options = {'quiet': ''}

def send_message(user_id, token, message=None, attachment="", random_id=0):
    api.messages.send(access_token=token, user_id=str(user_id), attachment=attachment, random_id=random_id)

def send_picture(user_id, token, q):
    filename = q.get()
    
    data = api.photos.getMessagesUploadServer(peer_id=user_id)

    DATA_GROUP_ID = data['group_id']
    DATA_UPLOAD_URL = data['upload_url']

    request = requests.post(DATA_UPLOAD_URL, files={'photo': open(filename, "rb")})

    params = {'server': request.json()['server'],
            'photo': request.json()['photo'],
            'hash': request.json()['hash'],
            'group_id': DATA_GROUP_ID
            }
    
    media_data = api.photos.saveMessagesPhoto(**params)

    photo_id = media_data[0]['id']
    owner_id = media_data[0]['owner_id']

    attach = 'photo' + str(owner_id) + '_' + str(photo_id)

    send_message(user_id, token, attachment=attach, random_id=randint(-100000, 100000))


def generate(lines, q, *args):
    i = args[0][0]
    word = args[0][1]
    url = pyqrcode.create(word)
    info = url.png_as_base64_str(10)
    
    lines = lines.substitute(qrcode=info)
    filename = 'coup{}.png'.format(i)
    imgkit.from_string(lines, filename, options=options)
    q.put(filename)

lines = ""

with open ('index.html', 'r') as fp:
    lines = fp.read()

lines = Template(lines)

m = Manager()
queue = m.Queue()
strt = time()
pool = Pool(processes=8)
pool.map(partial(generate, lines, queue), enumerate(text.split()))
while not queue.empty():
    send_picture(user_id, token, queue)
pool.close()
pool.join()

end = time()
print(end-strt)

